<!DOCTYPE HTML>
<html lang="cs">
<head>
    <title>Jaroslav Němec</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no"/>
    <link rel="stylesheet" href="../assets/css/mainForGallery.css"/>
    <link rel="stylesheet" href="../assets/css/flickity.css"/>

</head>

<body class="body-mill">

<header id="header" class="alt">
    <nav>
        <a style="color: #eeeeee" href="../index.html"><i class="fa fa-flag"></i>Home</a>
    </nav>
</header>

<h2 class="title"><u>Jawa Pionýr</u></h2>

<!--Carousel with some images from project-->
<div class="gallery js-flickity"
     data-flickity-options='{ "wrapAround": true }'>

    <?php

    $dir    = '../images/pionyr';
    $files = array_slice(scandir($dir),2);

    for ($i = 0; $i < count($files);$i++){
        $str = $files[$i];
        $len = strlen($str);

        $prefixLength = 3;//101...
        $suffixLength = 4;//suffix = .jpg
        $label = substr($str,$prefixLength,$len - $prefixLength - $suffixLength);

        echo('<div class="gallery-cell"><img alt = '.$label.' class="gallery-cell-img" src="'.$dir.'/'.$files[$i].'"><br>'.$label.'</div>');

    }
    ?>
</div>

<div></div>
<div></div>
<div></div>

<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/jquery.scrolly.min.js"></script>
<script src="../assets/js/jquery.scrollex.min.js"></script>
<script src="../assets/js/browser.min.js"></script>
<script src="../assets/js/breakpoints.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<script src="../assets/js/typewriter.js"></script>
<script src="../assets/js/AdsControl.js"></script>
<script src="../assets/js/flickity.pkgd.js"></script>

</body>
</html>