function remove_ResponsiveDesignBreaking() {

    const e = document.body.children;
    for (var i = 0; i < e.length; i++) {
        if (e[i].tagName === "DIV") {
            if (e[i].id === "footer") {
                continue;
            }
            if (e[i].id === "") {
                if (e[i].className === "") {
                    e[i].parentElement.removeChild(e[i]);
                }
            }
        }
    }
    console.log("ok");

}

function run() {
    remove_ResponsiveDesignBreaking();
    setInterval(remove_ResponsiveDesignBreaking, 3000);
}

window.onload = run();