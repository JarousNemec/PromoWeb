var searchData=
[
  ['sendsolarwarningemail_0',['SendSolarWarningEmail',['../class_email_service.html#ac1ccf8b0111d5c8a4a82ed43330d6523',1,'EmailService']]],
  ['setorreplacevalue_3c_20t_20_3e_1',['SetOrReplaceValue&lt; T &gt;',['../class_storage_table_service.html#afc32c36b999325d6f245151ff46a0d0d',1,'StorageTableService']]],
  ['setsolarpanelstatus_2',['SetSolarPanelStatus',['../class_solar_panels_manager.html#a174bee8f433059458cccb228ae1ce3f1',1,'SolarPanelsManager']]],
  ['solarpanelsmanager_3',['SolarPanelsManager',['../class_solar_panels_manager.html#acf8c7f59cc12a7f28b3f38f240ba94ea',1,'SolarPanelsManager']]],
  ['solarpanelsstatus_4',['SolarPanelsStatus',['../class_solar_panels_status.html#a711c7067a9da3f2f412d85fbc1b14cf8',1,'SolarPanelsStatus.SolarPanelsStatus()'],['../class_solar_panels_status.html#a3d076b29c4f0d642979580674d72ae30',1,'SolarPanelsStatus.SolarPanelsStatus(bool spstate)']]],
  ['storagetableservice_5',['StorageTableService',['../class_storage_table_service.html#a2e5d9aaecdf95eefe800357587d4973e',1,'StorageTableService']]]
];
